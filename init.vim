"nvim init
"Maintainer:
"	Alexander Daum
"Sections:
"	->Plugins ->Clang Format
"		->YouCompleteMe
"		->CoC Settings
"	->General
"	->NVIM UI
"	->Colors and Fonts
"	->Files
"	->Text tab and indent related
"	->Moving around tabs windows and buffers
"	->Edit mappings

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.config/nvim/plugged')
" General
Plug 'tpope/vim-fugitive'
Plug 'vim-scripts/argtextobj.vim'
Plug 'rhysd/vim-grammarous'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'vim-scripts/restore_view.vim'
"Programming
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'Chiel92/vim-autoformat'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'junegunn/vim-easy-align'
Plug 'Thyrum/vim-stabs'
Plug 'rhysd/vim-clang-format', {'for': ['c', 'cpp', 'objc', 'arduino']}
" Plug 'jackguo380/vim-lsp-cxx-highlight', {'for': ['c', 'cpp', 'objc', 'arduino']}
Plug 'cpiger/NeoDebug', {'for': ['c', 'cpp']}
Plug '456xander/vim-snippets'
"Latex
Plug 'donRaphaco/neotex', {'for': ['tex']}
Plug 'lervag/vimtex', {'for': ['tex']}
"Go
"Assembler
Plug 'ARM9/arm-syntax-vim'
Plug 'medvid/vim-armasm'
"TOML
Plug 'cespare/vim-toml'
call plug#end()

let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_method = 'zathura' 
let g:vimtex_fold_enabled = 1

autocmd FileType c,cpp call DoSetupKeybindC()

function! DoSetupKeybindC()
	map <C-B> :make<cr>
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = ","
let g:mapleader = ","

set number

":W for sudo saving
" command W w !sudo tee % > /dev/null


"Latex

nnoremap <leader>vco :VimtexCompile<cr>
nnoremap <leader>vcl :VimtexClean<cr>
nnoremap <leader>vvi :VimtexView<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Auto Format
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <leader>cf :Autoformat<cr>

autocmd FileType c,cpp,objc,arduino let g:clang_format#style_options = {
			\ "IndentWidth": 4,
			\ "TabWidth" : 4,
			\ "UseTab" : "Always" }



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Config for Easy-Align
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"CoC Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"Make it compatible with stabs
let g:stabs_maps = 'tboO='



" " Use tab for trigger completion with characters ahead and navigate.
" inoremap <silent><expr> <TAB>
"			\ pumvisible() ? "\<C-n>" :
"           \ <SID>check_back_space() ? StabsTab():
"			\ coc#refresh()

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter.
inoremap <silent><expr> <CR> pumvisible() ? coc#_select_confirm()
			\: "\<CR>"


" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

set signcolumn=number

function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <c-space> coc#refresh()

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
	if (index(['vim','help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	elseif (coc#rpc#ready())
		call CocActionAsync('doHover')
	else
		execute '!' . &keywordprg . " " . expand('<cword>')
	endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn :CocCommand document.renameCurrentWord<cr>

augroup mygroup
	autocmd!
	" Setup formatexpr specified filetype(s).
	autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
	" Update signature help on jump placeholder.
	autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
	nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
	nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
	inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
	inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
	vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
	vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'

" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Treesitter
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

lua <<+
require'nvim-treesitter.configs'.setup {
ensure_installed = "all", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
highlight = {
enable = true,              -- false will disable the whole extension
-- Setting this to true will run `:h syntax` and tree-sitter at the same time.
-- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
-- Using this option may slow down your editor, and you may see some duplicate highlights.
-- Instead of true it can also be a list of languages
additional_vim_regex_highlighting = false
},
}
+


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"NVIM UI
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set wildignore +=*.o,*.swp
"Ignore case when searching, but use case when using capital letters
set ignorecase
set smartcase
"Timeout after 2s when making command with multiple key presses
set timeout
set timeoutlen=2000
set laststatus=2
map <leader>hn :nohls<CR>

fun! SetFoldmethod()
	let g:tex_fold_enabled=1
	if &ft == 'rust'
		setlocal foldmethod=indent
	elseif &ft == 'tex'
		" setlocal foldmethod=expr
		" setlocal foldexpr=vimtex#fold#level(v:lnum)
		" setlocal foldtext=vimtex#fold#text()
	else
		setlocal foldmethod=syntax
	endif
endfun

autocmd FileType call SetFoldmethod()

set foldcolumn=2

set viewoptions-=options

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
try
	colorscheme molokai
catch
endtry
"set t_Co=256
set termguicolors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set encoding=utf8

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:tex_flavor = 'latex'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Text tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Be smart using tabs
set smarttab
"Break too long lines at words, not anywhere
set linebreak

"One tab has width of 4, use width 4 for indent
set shiftwidth=4
set tabstop=4
inoremap {<space>	{}<Left>
inoremap {<CR>	{<CR>}<Esc>O
inoremap {{	{
	inoremap{} {}
	inoremap (<space>		()<Left>
	inoremap ()		()
	inoremap [<space>		[]<Left>
	inoremap []		[]

	function! HtmlKlammern()
		inoremap <<space>	<><Left>
		inoremap <>	<>
	endfunction

	"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	"Moving Around tabs windows and buffers
	"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	"Smart way to move between windows
	nnoremap <C-j> <C-W>j
	nnoremap <C-k> <C-W>k
	nnoremap <C-h> <C-W>h
	nnoremap <C-l> <C-W>l

	"useful tab mappings
	map <leader>tc :tabclose<cr>gT
	map <leader>tn :tabnew<cr>
	map <leader>tj :tabnext<cr>
	map <leader>tk :tabnext -1<cr>
	"Close all buffers
	map <leader>ba :bufdo bd<cr>

	"Let 'tl' toggle between this and the last accessed tab
	let g:lasttab = 1
	nmap <leader>tl :exe "tabn ".g:lasttab<cr>
	au TabLeave * let g:lasttab = tabpagenr()

	"Opens a new tab with the directory of the current buffer
	map <leader>te :tabedit <c-r>=expand("%:p:h")<cr><cr>

	"switch cwd to the directory of the buffer
	map <leader>cd :cd %:p:h<cr>:pwd<cr>

	"Return to the last edit position when opening files
	au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

	"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	"Editing Mappings
	"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	"Move lines around using ALT+[jk], need xterm MetaSendsEscape!
	nmap <M-j> mz:m+<cr>
	nmap <M-k> mz:m-2<cr>

	" Pressing ,ss will toggle and untoggle spell checking
	map <leader>ss :setlocal spell!<cr>

	" Shortcuts using <leader>

	function DebugArm()
		let g:neodbg_gdb_path = "/usr/bin/arm-none-eabi-gdb"
	endfunction

	nnoremap <leader>dbg :NeoDebug exe_debug<cr>
	nnoremap <leader>sbg :NeoDebugStop<cr>

	"Exit Terminal mode with esc

	tnoremap <esc> <C-\><C-n>
