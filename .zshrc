# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored _approximate
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+r:|[._-]=** r:|=**' 'l:|=* r:|=*'
zstyle ':completion:*' max-errors 1
zstyle ':completion:*' prompt 'Do you mean?'
zstyle :compinstall filename '/home/alex/.zshrc'

autoload -Uz compinit promptinit
promptinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
alias ls="ls --color=always"
alias grep="grep --color=always"
alias suspend="systemctl suspend"
alias devstats="~/node_modules/devstats/bin/devstats"
alias pacu="pacaur -Syu"
alias btrfs_compress="btrfs filesystem defragment -czstd"
alias java8="/usr/lib/jvm/java-8-openjdk/bin/java"
alias greadd="git restore --staged . && git add ."
alias gst="git status"
alias glo="git log --oneline"
alias glg="git log --oneline --decorate --graph"
alias glag="git log --oneline --decorate --all --graph"
alias emacs="emacsclient -c -a ''"
alias gpu="git push"
alias studium="cd ~/Documents/Studium/SEM5"
alias o="xdg-open"
alias yay=paru
export EDITOR=$(which nvim)

export ZSH_CACHE_DIR=~/.cache/zsh

#=========================
#Load Plugins using ZPlug
#=========================

source ~/.zplug/init.zsh
zplug "woefe/git-prompt.zsh"
zplug "jeffreytse/zsh-vi-mode"

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load

#=========================
#     End Plugins
#=========================

#=========================
#         Misc
#=========================

#Use mkcd to create dir and cd to it
mkcd() {
	mkdir -p $1 && cd $1
}

soc_kernel() {
	export ARCH=arm
	export CROSS_COMPILE=arm-linux-gnueabihf-
}


# key bindings

export GOPATH=$HOME/gopath
export PATH=$HOME/.cargo/bin:$HOME/.local/bin:$GOPATH:$GOPATH/bin:$PATH:
PATH=$PATH:/opt/intelFPGA/default/quartus/bin/
PATH=$PATH:/opt/intelFPGA/default/modelsim_ase/bin/
PATH=$PATH:/opt/intel/oneapi/vtune/latest/bin64/:/opt/intel/oneapi/inspector/latest/bin64
export VUNIT_SIMULATOR=ghdl

alias quartus="quartus --64bit"

source '/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh'
source '/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh'
source "$HOME/Software/Scripts/lwd.zsh"

export QSYS_ROOTDIR="/opt/intelFPGA/21.1/quartus/sopc_builder/bin"

PATH="/home/alex/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/alex/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/alex/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/alex/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/alex/perl5"; export PERL_MM_OPT;
